use clap::{Arg, Command};

fn main() {
    let m = Command::new("Kv Client")
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("This is a Kv Client")
        .subcommand(
            Command::new("set")
                .about("Set the value of a string key to a string")
                .arg(
                    Arg::new("TABLE")
                        .help("set the name of table")
                        .required(true),
                )
                .arg(Arg::new("KEY").help("A string key").required(true))
                .arg(
                    Arg::new("VALUE")
                        .help("The string value of the key")
                        .required(true),
                ),
        )
        .get_matches();

    match m.subcommand() {
        Some(("set", matches)) => {
            //unimplemented!();
            let key = matches.value_of("KEY").unwrap();
            let value = matches.value_of("VALUE").unwrap();
            let table = matches.value_of("TABLE").unwrap();
            println!("{:?} -> {:?}:{:?}", table, key, value);
        }
        _ => {
            unimplemented!();
        }
    }
}
