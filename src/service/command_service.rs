use crate::*;

impl CommandService for Hget {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        match store.get(&self.table, &self.key) {
            Ok(Some(v)) => v.into(),
            Ok(None) => KvError::NotFound(self.table, self.key).into(),
            Err(e) => e.into(),
        }
    }
}

impl CommandService for Hgetall {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        match store.get_all(&self.table) {
            Ok(v) => v.into(),
            Err(e) => e.into(),
        }
    }
}

impl CommandService for Hmget {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        let mut ret = vec![];
        for key in self.keys {
            let cmd = CommandRequest::new_hget(&self.table, key);
            let res = dispatch(cmd, store);
            if res.status == 200 && res.value.len() == 1 {
                ret.push(res.value[0].clone());
            } else {
                return res;
            }
        }

        ret.into()
    }
}

impl CommandService for Hset {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        match self.pair {
            Some(v) => match store.set(&self.table, v.key, v.value.unwrap_or_default()) {
                Ok(Some(v)) => v.into(),
                Ok(None) => Value::default().into(),
                Err(e) => e.into(),
            },
            None => Value::default().into(),
        }
    }
}

impl CommandService for Hmset {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        // 设置好所有的值，返回一个 true
        for kvpair in self.pairs {
            let cmd = CommandRequest::new_hset(&self.table, kvpair.key, kvpair.value.unwrap());
            let resp = dispatch(cmd, store);
            if resp.status != 200 {
                return resp;
            }
        }
        true.into()
    }
}

impl CommandService for Hdel {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        match store.del(&self.table, &self.key) {
            Ok(Some(ret)) => ret.into(),
            Ok(None) => KvError::NotFound(self.table, self.key).into(),
            Err(e) => e.into(),
        }
    }
}

impl CommandService for Hmdel {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        let mut ret = vec![];
        for key in &self.keys {
            let cmd = CommandRequest::new_hdel(&self.table, key);
            let res = dispatch(cmd, store);
            if res.status == 200 && res.value.len() == 1 {
                ret.push(res.value[0].clone());
            } else {
                return res;
            }
        }

        ret.into()
    }
}

impl CommandService for Hexist {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        match store.contains(&self.table, &self.key) {
            Ok(ret) => {
                if ret == true {
                    ret.into()
                } else {
                    KvError::NotFound(self.table, self.key).into()
                }
            }
            Err(e) => e.into(),
        }
    }
}

impl CommandService for Hmexist {
    fn execute(self, store: &impl Storage) -> CommandResponse {
        for key in &self.keys {
            let cmd = CommandRequest::new_hexist(&self.table, key);
            let res = dispatch(cmd, store);
            if res.status != 200 {
                return res;
            }
        }

        true.into()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::command_request::RequestData;

    #[test]
    fn hset_should_work() {
        let store = MemTable::new();
        let cmd = CommandRequest::new_hset("t1", "hello", "world".into());
        let res = dispatch(cmd.clone(), &store);
        assert_res_ok(res, &[Value::default()], &[]);

        let res = dispatch(cmd, &store);
        assert_res_ok(res, &["world".into()], &[]);
    }

    #[test]
    fn hmset_should_work() {
        let store = MemTable::new();
        let kvpairs = vec![
            Kvpair::new("key1", "value1".into()),
            Kvpair::new("key2", "value2".into()),
            Kvpair::new("key3", "value3".into()),
        ];
        let cmd = CommandRequest::new_hmset("table1", kvpairs);
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[true.into()], &[]);

        // 看看能不能 get 出来
        for i in 1..4 {
            let key = format!("key{}", i);
            let cmd = CommandRequest::new_hget("table1", key);
            let res = dispatch(cmd, &store);
            assert_res_ok(res, &[format!("value{}", i).into()], &[]);
        }
    }

    #[test]
    fn hget_should_work() {
        let store = MemTable::new();
        let cmd = CommandRequest::new_hset("score", "u1", 10.into());
        dispatch(cmd, &store);

        let cmd = CommandRequest::new_hget("score", "u1");
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[10.into()], &[]);
    }

    #[test]
    fn hget_with_non_exist_key_should_return_404() {
        let store = MemTable::new();
        let cmd = CommandRequest::new_hget("score", "u1");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    #[test]
    fn hmget_should_work() {
        let store = MemTable::new();
        let kvpairs = vec![
            Kvpair::new("key1", "apple".into()),
            Kvpair::new("key2", "banana".into()),
            Kvpair::new("key3", "cheery".into()),
        ];
        let cmd = CommandRequest::new_hmset("table1", kvpairs);
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[true.into()], &[]);

        let cmd = CommandRequest::new_hmget(
            "table1",
            vec!["key1".to_string(), "key2".to_string(), "key3".to_string()],
        );
        let res = dispatch(cmd, &store);
        assert_res_ok(
            res,
            &["apple".into(), "banana".into(), "cheery".into()],
            &[],
        );

        // key 不存在会报错
        let cmd = CommandRequest::new_hmget(
            "table1",
            vec![
                "key1".to_string(),
                "key2".to_string(),
                "key_not_exist".to_string(),
            ],
        );
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");

        // table 不存在会报错
        let cmd = CommandRequest::new_hmget(
            "table_not_exist",
            vec![
                "key1".to_string(),
                "key2".to_string(),
                "key_not_exist".to_string(),
            ],
        );
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    #[test]
    fn hgetall_should_work() {
        let store = MemTable::new();
        let cmds = vec![
            CommandRequest::new_hset("score", "u1", 10.into()),
            CommandRequest::new_hset("score", "u2", 8.into()),
            CommandRequest::new_hset("score", "u3", 11.into()),
            CommandRequest::new_hset("score", "u1", 6.into()),
        ];
        for cmd in cmds {
            dispatch(cmd, &store);
        }

        let cmd = CommandRequest::new_hgetall("score");
        let res = dispatch(cmd, &store);
        let pairs = &[
            Kvpair::new("u1", 6.into()),
            Kvpair::new("u2", 8.into()),
            Kvpair::new("u3", 11.into()),
        ];
        assert_res_ok(res, &[], pairs);
    }

    #[test]
    fn hdel_should_work() {
        let store = MemTable::new();
        let cmds = vec![
            CommandRequest::new_hset("table1", "key1", "value1".into()),
            CommandRequest::new_hset("table1", "key2", "value2".into()),
            CommandRequest::new_hset("table1", "key3", "value3".into()),
        ];
        for cmd in cmds {
            dispatch(cmd, &store);
        }

        // 删除存在的 key 能成功, 返回它之前的值
        let cmd = CommandRequest::new_hdel("table1", "key1");
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &["value1".into()], &[]);

        let cmd = CommandRequest::new_hdel("table1", "key2");
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &["value2".into()], &[]);

        // 删除不存在的 key 会失败
        let cmd = CommandRequest::new_hdel("table1", "key1");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");

        // 在错误的表中执行 HDEL 操作会失败
        let cmd = CommandRequest::new_hdel("table_not_exist", "key3");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    #[test]
    fn hmdel_should_work() {
        let store = MemTable::new();
        let cmds = vec![
            CommandRequest::new_hset("table1", "key1", "apple".into()),
            CommandRequest::new_hset("table1", "key2", "banana".into()),
            CommandRequest::new_hset("table1", "key3", "cheery".into()),
        ];
        for cmd in cmds {
            dispatch(cmd, &store);
        }

        // get 能成功
        let cmd = CommandRequest::new_hget("table1", "key1");
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &["apple".into()], &[]);

        let cmd =
            CommandRequest::new_hmdel("table1", vec!["key1".into(), "key2".into(), "key3".into()]);
        let res = dispatch(cmd, &store);
        assert_res_ok(
            res,
            &["apple".into(), "banana".into(), "cheery".into()],
            &[],
        );

        // get 会失败
        let cmd = CommandRequest::new_hget("table1", "key1");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    #[test]
    fn hexist_should_work() {
        let store = MemTable::new();
        let cmd = CommandRequest::new_hset("table1", "key1", "value1".into());
        dispatch(cmd, &store);

        // key 存在 返回 true
        let cmd = CommandRequest::new_hexist("table1", "key1");
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[true.into()], &[]);

        // key 不存在返回 false
        let cmd = CommandRequest::new_hexist("table1", "key_not_exist");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");

        // table 不存在返回 false
        let cmd = CommandRequest::new_hexist("table_not_exist", "key1");
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    #[test]
    fn hmexist_should_work() {
        let store = MemTable::new();
        let kvpairs = vec![
            Kvpair::new("key1", "value1".into()),
            Kvpair::new("key2", "value2".into()),
            Kvpair::new("key3", "value3".into()),
        ];
        let cmd = CommandRequest::new_hmset("table1", kvpairs);
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[true.into()], &[]);

        // key 存在 返回 true
        let cmd = CommandRequest::new_hmexist(
            "table1",
            vec!["key1".to_string(), "key2".to_string(), "key3".to_string()],
        );
        let res = dispatch(cmd, &store);
        assert_res_ok(res, &[true.into()], &[]);

        // key 不存在返回 false
        let cmd = CommandRequest::new_hmexist(
            "table1",
            vec![
                "apple".to_string(),
                "banana".to_string(),
                "cheery".to_string(),
            ],
        );
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");

        // table 不存在返回 false
        let cmd = CommandRequest::new_hmexist(
            "table_not_exist",
            vec!["key1".to_string(), "key2".to_string(), "key3".to_string()],
        );
        let res = dispatch(cmd, &store);
        assert_res_error(res, 404, "Not found");
    }

    // 从 Request 中得到 Response，目前处理 HGET/HGETALL/HSET
    fn dispatch(cmd: CommandRequest, store: &impl Storage) -> CommandResponse {
        match cmd.request_data.unwrap() {
            RequestData::Hget(v) => v.execute(store),
            RequestData::Hgetall(v) => v.execute(store),
            RequestData::Hmget(v) => v.execute(store),
            RequestData::Hset(v) => v.execute(store),
            RequestData::Hmset(v) => v.execute(store),
            RequestData::Hdel(v) => v.execute(store),
            RequestData::Hmdel(v) => v.execute(store),
            RequestData::Hexist(v) => v.execute(store),
            RequestData::Hmexist(v) => v.execute(store),
            //_ => todo!(),
        }
    }

    // 测试成功返回的结果
    fn assert_res_ok(mut res: CommandResponse, values: &[Value], pairs: &[Kvpair]) {
        res.pairs.sort_by(|a, b| a.partial_cmp(b).unwrap());
        assert_eq!(res.status, 200);
        assert_eq!(res.message, "");
        assert_eq!(res.value, values);
        assert_eq!(res.pairs, pairs);
    }

    // 测试失败返回的结果
    fn assert_res_error(res: CommandResponse, code: u32, msg: &str) {
        assert_eq!(res.status, code);
        assert!(res.message.contains(msg));
        assert_eq!(res.value, &[]);
        assert_eq!(res.pairs, &[]);
    }
}
