use anyhow::Result;
use clap::{Arg, ArgMatches, Command};
use kv_server::{CommandRequest, ProstClientStream};
use tokio::net::TcpStream;
use tracing::info;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    let addr = "127.0.0.1:9527";
    // 连接服务器
    let stream = TcpStream::connect(addr).await?;
    let mut client = ProstClientStream::new(stream);

    let m = init_args();
    match_request(&m, &mut client).await?;

    Ok(())
}

fn init_args() -> ArgMatches {
    let m = Command::new("Kv Client")
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about("This is a Kv Client")
        .subcommand(
            Command::new("set")
                .about("Set the value of a string key to a string")
                .arg(
                    Arg::new("TABLE")
                        .help("set the name of table")
                        .required(true),
                )
                .arg(Arg::new("KEY").help("A string key").required(true))
                .arg(
                    Arg::new("VALUE")
                        .help("The string value of the key")
                        .required(true),
                ),
        )
        .subcommand(
            Command::new("get")
                .about("Get the string value of a given string key")
                .arg(
                    Arg::new("TABLE")
                        .help("set the name of table")
                        .required(true),
                )
                .arg(Arg::new("KEY").help("A string key").required(true)),
        )
        .subcommand(
            Command::new("del")
                .about("Remove a given key")
                .arg(
                    Arg::new("TABLE")
                        .help("set the name of table")
                        .required(true),
                )
                .arg(Arg::new("KEY").help("A string key").required(true)),
        )
        .subcommand(
            Command::new("exist")
                .about("To check if the key is exists")
                .arg(
                    Arg::new("TABLE")
                        .help("set the name of table")
                        .required(true),
                )
                .arg(Arg::new("KEY").help("A string key").required(true)),
        )
        .subcommand(
            Command::new("getall").about("Get all keys").arg(
                Arg::new("TABLE")
                    .help("set the name of table")
                    .required(true),
            ),
        )
        .get_matches();

    m
}

async fn match_request(m: &ArgMatches, client: &mut ProstClientStream<TcpStream>) -> Result<()> {
    match m.subcommand() {
        Some(("set", matches)) => {
            let table = matches.value_of("TABLE").unwrap();
            let key = matches.value_of("KEY").unwrap();
            let value = matches.value_of("VALUE").unwrap();
            let cmd = CommandRequest::new_hset(table, key, value.into());

            // 发送命令
            let data = client.execute(cmd).await?;
            info!("Got response {:?}", data);
        }
        Some(("get", matches)) => {
            let table = matches.value_of("TABLE").unwrap();
            let key = matches.value_of("KEY").unwrap();
            let cmd = CommandRequest::new_hget(table, key);

            // 发送命令
            let data = client.execute(cmd).await?;
            info!("Got response {:?}", data);
        }
        Some(("del", matches)) => {
            let table = matches.value_of("TABLE").unwrap();
            let key = matches.value_of("KEY").unwrap();
            let cmd = CommandRequest::new_hdel(table, key);

            // 发送命令
            let data = client.execute(cmd).await?;
            info!("Got response {:?}", data);
        }
        Some(("exist", matches)) => {
            let table = matches.value_of("TABLE").unwrap();
            let key = matches.value_of("KEY").unwrap();
            let cmd = CommandRequest::new_hexist(table, key);

            // 发送命令
            let data = client.execute(cmd).await?;
            info!("Got response {:?}", data);
        }
        Some(("getall", matches)) => {
            let table = matches.value_of("TABLE").unwrap();
            let cmd = CommandRequest::new_hgetall(table);

            // 发送命令
            let data = client.execute(cmd).await?;
            info!("Got response {:?}", data);
        }
        _ => {
            unimplemented!();
        }
    }
    Ok(())
}
